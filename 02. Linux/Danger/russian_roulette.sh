#!/usr/bin/bash

# BEWARE! THIS SCRIPT WILL KILL PROCESSES
# AT RANDOM UNTIL IT CRASHES YOUR MACHINE
# OR YOU GIVE UP. USE AT YOUR OWN RISKS!

# Author: Silejonu
# E-mail: silejonu@tutanota.com
# Date: 2023-05-15
# Source: https://codeberg.org/Silejonu/miscellaneous_scripts

if [[ $UID -ne 0 ]] ; then
  echo 'Only a weakling would play the russian roulette this way…'
  echo 'Come on, run this script with superuser privileges!'
  exit 2
fi

while true ; do
  target=$(ps --no-headers -eo pid,comm | grep -Ev 'russian_roulette.sh|ps --no-headers -eo pid,comm|shuf -n1' | shuf -n1)
  kill "$(echo "${target}" | awk '{print $1}')"
  echo
  echo "$(echo "${target}" | awk '{print $2}') has been shot dead."
  read -rp 'Dare to pull the trigger one more time? [Y/n] ' yesno
  case ${yesno} in
    [nN]|[nN][oO]) echo && echo 'A bit of a coward, I see…' && exit 0 ;;
    *) ;;
  esac
done