#!/usr/bin/bash

# Install Microsoft fonts on a Linux system by extracting them from a Windows 10/11 ISO

# How to use:
# 1. Download a Windows ISO from https://www.microsoft.com/software-download/windows11
# 2. Run the script

# Author: Silejonu
# E-mail: silejonu@tuta.com
# Date: 2024-07-04
# Source: https://codeberg.org/Silejonu/miscellaneous_scripts

if [[ ${UID} == 0 ]] ; then
  echo
  echo 'Error: This script must not be ran with super-user privileges.'
  exit 2
fi

if ! ls Win1*.iso &> /dev/null ; then
  echo
  echo 'Error: please place a Windows 10 or 11 ISO in this directory.'
  exit 3
fi

if ! which 7z &> /dev/null ; then
  cat << EOF
Error: missing dependency: 7z
Please install it and re-run the script.

For Red Hat family (the EPEL repo is required):
sudo dnf install p7zip-plugins

For Debian/Ubuntu families:
sudo apt install p7zip-full

For Arch Linux family:
sudo pacman -S p7zip

For openSUSE Leap:
sudo zypper install p7zip-full
EOF
  exit 4
fi

7z e Win1*.iso sources/install.wim -o/tmp/
7z e /tmp/install.wim 1/Windows/Fonts/"*".{ttf,ttc} -o/tmp/MicrosoftFonts
rm /tmp/install.wim

sudo mkdir -p /usr/local/share/fonts/MicrosoftFonts
sudo mv /tmp/MicrosoftFonts/* /usr/local/share/fonts/MicrosoftFonts/
sudo chown -R root:root /usr/local/share/fonts/MicrosoftFonts
sudo chmod 755 /usr/local/share/fonts/MicrosoftFonts
sudo chmod 644 /usr/local/share/fonts/MicrosoftFonts/*

if which restorecon &> /dev/null ; then
  sudo restorecon -Fr /usr/local/share/fonts
fi

sudo fc-cache

if [[ "$?" -eq 0 ]] ; then
  echo
  echo 'Windows fonts installed successfully.'
fi